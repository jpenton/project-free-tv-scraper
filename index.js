const axios = require('axios');
const cheerio = require('cheerio');
const commander = require('commander');

const BASE_URL = 'https://www8.project-free-tv.ag';
const URL = '/episode/blindspot-season-4-episode-2/';
const BLACKLISTED_DOMAINS = [];

commander.option('-u, --url [address]', 'URL to scrape').parse(process.argv);

const getVideos = async url => {
  const { data } = await axios.get(url);
  const $ = cheerio.load(data);

  const links = $('#mybox > table > tbody > tr > td:nth-child(1) > a').slice(1);
  const mappedLinks = Array.from(links)
    .map(({ attribs: { href }, children: [tag, { data: text }] }) => ({
      domain: text.trim(),
      url: `https://${url.split('/')[2]}${href}`,
    }))
    .filter(link => !BLACKLISTED_DOMAINS.includes(link.domain));

  return mappedLinks;
};

const main = async () => {
  const links = await getVideos(commander.url);

  console.log(links.map(link => link.url).join('\n'));
};

main();
